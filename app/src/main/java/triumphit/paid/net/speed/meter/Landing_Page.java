package triumphit.paid.net.speed.meter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pushbots.push.Pushbots;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Landing_Page extends AppCompatActivity {

    GoogleApiClient mGoogleApiClient;
    static SharedPreferences sp;
    static SharedPreferences.Editor editor;
    static TextView actionBarUsedText, actionBarAvailabeText, actionBarTotalText, actionBarDateText, appName, appVersion;
    static View mCustomView;
    Tracker realTime;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    int pos = 0;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;


    private static final int REQUEST_CODE_CAPTURE_IMAGE = 1;
    private static final int REQUEST_CODE_CREATOR = 2;
    private static final int REQUEST_CODE_RESOLUTION = 3;
    String accountName = "not set";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_holder);
        sp = getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Net Speed Meter");


        LayoutInflater mInflater = LayoutInflater.from(this);
        mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        getSupportActionBar().setCustomView(mCustomView);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        actionBarUsedText = (TextView) mCustomView.findViewById(R.id.textView5);
        actionBarTotalText = (TextView) mCustomView.findViewById(R.id.textView4);
        actionBarAvailabeText = (TextView) mCustomView.findViewById(R.id.textView6);
        actionBarDateText = (TextView) mCustomView.findViewById(R.id.textView7);

        appName = (TextView) findViewById(R.id.username);
        appVersion = (TextView) findViewById(R.id.email);
        try {
            appVersion.setText("Version: " + this.getPackageManager()
                    .getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            appVersion.setText("Not detected");
        }

        Typeface type = Typeface.createFromAsset(getAssets(),"Capture it.ttf");
        appName.setTypeface(type);
        appVersion.setTypeface(type);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff) ;
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new TabFragment()).commit();
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

                if (menuItem.getItemId() == 1) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.containerView,new TabFragment()).commit();

                }

                if (menuItem.getItemId() == 2) {
                    FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                    xfragmentTransaction.replace(R.id.containerView,new TabFragment()).commit();
                }

                return false;
            }

        });

        final List<MenuItem> items=new ArrayList<>();
        Menu menu;
        menu = mNavigationView.getMenu();
        for(int i = 0; i < menu.size(); i++){
            items.add(menu.getItem(i));
        }
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if(menuItem.getItemId() == R.id.rate){
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
                if(menuItem.getItemId() == R.id.about){
                    startActivity(new Intent(Landing_Page.this, About.class));
                }
                return false;
            }
        });

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout, toolbar,R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);

        realTime = ((Analytics) getApplication()).getTracker(Analytics.TrackerName.APP_TRACKER);
        realTime.setScreenName("Main");
        realTime.send(new HitBuilders.AppViewBuilder().build());
        realTime.enableAdvertisingIdCollection(true);
        realTime.enableExceptionReporting(true);
        realTime.setAnonymizeIp(true);

        Account account = getAccount(AccountManager.get(getApplicationContext()));

        if (account == null) {
            accountName = "notSet";
        } else {
            accountName = account.name;
        }

        Pushbots.sharedInstance().init(this);
        Pushbots.sharedInstance().setCustomHandler(PushCustomHandler.class);
        Pushbots.sharedInstance().setAlias(accountName + " " + Build.DEVICE + " " + Build.MODEL + " Dual " + telephonyInfo.isDualSIM());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();
        startService(new Intent(getApplicationContext(), Background.class));
    }

    public static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(Landing_Page.this, About.class));
            return true;
        }
        if (id == R.id.action_rate) {
            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
            return true;
        }
        if(id == android.R.id.home){
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)){
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        setActionBarTitle(pos);
        super.onStart();
        GoogleAnalytics.getInstance(Landing_Page.this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(Landing_Page.this).reportActivityStop(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            pos = 0;
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd");
            String formattedDate = df.format(c.getTime());
            int date = Integer.parseInt(formattedDate);
            if (date % sp.getInt("reviewCycle", 3) == 0) {
                if (sp.getBoolean("review", true)) {
                    editor.putBoolean("review", false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setTitle("RATE NET SPEED METER")
                            .setMessage("Please rate this app. Your review will help us to improve our NET SPEED METER.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                    editor.putInt("reviewCycle", 30);// do nothing
                                    editor.commit();
                                    finish();
                                }
                            })
                            .setNegativeButton("Never", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    editor.putInt("reviewCycle", 20);// do nothing
                                    editor.commit();
                                    finish();
                                }
                            }).setNeutralButton("Later", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            editor.putInt("reviewCycle", 3);// do nothing
                            editor.commit();
                            finish();
                        }
                    })
                            .setIcon(R.drawable.alert)
                            .show();

                    editor.commit();

                } else {
                    finish();
                }

                return true;
            }else {
                editor.putBoolean("review", true);
                editor.commit();
            }
            onBackPressed();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void setActionBarTitle(int pos) {
        if (pos == 0) {
            long limit = sp.getLong("dataLimit_mobile", 0) / 1000;
            long used = sp.getLong("totalMob", 0) / 1000;
            if (used > 1000) {
                double d = used / 1000.0;
                float usedPercent = getPercentage(d, limit);
                float availablePercent = 100.00f - usedPercent;
                actionBarUsedText.setText("Used: " + String.format("%.1f", usedPercent) + "%");
                actionBarAvailabeText.setText("Available: " + String.format("%.1f", availablePercent) + "%");
            } else {
                float usedPercent = getPercentage(used, limit);
                float availablePercent = 100.00f - usedPercent;
                actionBarUsedText.setText("Used: " + String.format("%.1f", usedPercent) + "%");
                actionBarAvailabeText.setText("Available: " + String.format("%.1f", availablePercent) + "%");
            }


            if (limit > 1000) {
                double d = limit / 1000.0;
                actionBarTotalText.setText("Total: " + d + "GB.");
            } else if ((limit - used) < 0) {
                actionBarAvailabeText.setText("Available: Over");
                actionBarUsedText.setText("Used: All");
            } else {
                actionBarTotalText.setText("Total: " + limit + " MB.");
            }
            actionBarDateText.setText("Expire: " + sp.getString("dataDuration_mobile", "NO Limit"));
        } else if (pos == 1) {
            long limit = sp.getLong("dataLimit_wifi", 0) / 1000;
            long used = sp.getLong("totalWifi", 0);
            if (used > 1000) {
                double d = used / 1000.0;
                float usedPercent = getPercentage(d, limit);
                float availablePercent = 100.00f - usedPercent;
                actionBarUsedText.setText("Used: " + String.format("%.1f", usedPercent) + "%");
                actionBarAvailabeText.setText("Available: " + String.format("%.1f", availablePercent) + "%");
            } else {
                float usedPercent = getPercentage(used, limit);
                float availablePercent = 100.00f - usedPercent;
                actionBarUsedText.setText("Used: " + String.format("%.1f", usedPercent) + "%");
                actionBarAvailabeText.setText("Available: " + String.format("%.1f", availablePercent) + "%");
            }

            if (limit > 1000) {
                double d = limit / 1000.0;
                actionBarTotalText.setText("Total: " + d + "GB.");
            } else {
                actionBarTotalText.setText("Total: " + limit + " MB.");
            }
            actionBarDateText.setText("Expire: " + sp.getString("dataDuration_wifi", "NO Limit"));
        } else {
            long limit = sp.getLong("dataLimit_both", 0) / 1000;
            long used = (sp.getLong("totalMob", 0)) + (sp.getLong("totalWifi", 0)) / 1000;
            if (used > 1000) {
                double d = used / 1000.0;
                actionBarUsedText.setText("Used: " + getPercentage(d, limit) + "GB.");
            } else {
                actionBarUsedText.setText("Used: " + getPercentage(used, limit) + "MB.");
            }

            if ((limit - used) > 1000) {
                double d = (limit - used) / 1000.0;
                actionBarAvailabeText.setText("Availabe: " + getPercentage(d, limit) + "GB.");
            } else {
                actionBarAvailabeText.setText("Availabe: " + getPercentage((limit - used), limit) + " MB.");
            }

            if (limit > 1000) {
                double d = limit / 1000.0;
                actionBarTotalText.setText("Total: " + d + "GB.");
            } else {
                actionBarTotalText.setText("Total: " + limit + " MB.");
            }
            actionBarDateText.setText("Expire: " + sp.getString("dataDuration_both", "NO Limit"));

        }
    }

    public static float getPercentage(double n, long total) {
        float proportion = ((float) n) / ((float) total);
        return proportion * 100;
    }

}
