package triumphit.paid.net.speed.meter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

public class Details extends AppCompatActivity {

    private float[] yData= {25, 25, 25, 25};
    private String[] xData= {"wifi download 70%", "wifi upload 9%", "mobile download 11%", "mobile upload 10%" };
    private PieChart pChart;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    TextView total, dDown, dUp, wUp;
    //Tracker realTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        final TelephonyInfo telephonyInfo = TelephonyInfo.getInstance(this);
        Account account = getAccount(AccountManager.get(getApplicationContext()));
        final String accountName;
        if(account == null){
            accountName = "notSet";
        } else{
            accountName = account.name;
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00A5EC")));
        getSupportActionBar().setTitle("Details");


        sp = getSharedPreferences("stat", Context.MODE_PRIVATE);
        editor = sp.edit();
        total = (TextView) findViewById(R.id.textView8);
        dDown= (TextView) findViewById(R.id.textView19);
        dUp = (TextView) findViewById(R.id.textView18);
        wUp = (TextView) findViewById(R.id.textView20);

//        realTime = ((Analytics) getApplication()).getTracker(Analytics.TrackerName.APP_TRACKER);
//        realTime.setScreenName("Details");
//        realTime.send(new HitBuilders.AppViewBuilder().build());
//        realTime.enableAdvertisingIdCollection(true);
//        realTime.enableAutoActivityTracking(true);
//        realTime.enableExceptionReporting(true);
//        realTime.setAnonymizeIp(true);

        calculateHistoryPercentage();

        pChart = (PieChart) findViewById(R.id.chart2);
        pChart.setDrawHoleEnabled(true);
        pChart.setDrawSliceText(false);
        pChart.setHoleColorTransparent(true);
        pChart.setHoleRadius(7);
        pChart.setTransparentCircleRadius(20);
        pChart.setRotationAngle(0);
        pChart.setRotationEnabled(true);
        pChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                if (e == null)
                    return;
                Log.e("Chart Clicked", "" + xData[e.getXIndex()] + " = " + e.getVal());
            }

            @Override
            public void onNothingSelected() {

            }
        });
        addData();
        Legend lg = pChart.getLegend();
        lg.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        lg.setTextColor(Color.BLACK);
        lg.setXEntrySpace(7);
        lg.setYEntrySpace(5);
    }

    private void calculateHistoryPercentage() {
        long total = (sp.getLong("totalUpWifi1", 0) + sp.getLong("totalUpMob1", 0) + sp.getLong("totalDownWifi1", 0) + sp.getLong("totalDownMob1", 0)) / 1000;

        long totalDownWifi = sp.getLong("totalDownWifi1", 0) / 1000;
        long totalUpWifi = sp.getLong("totalUpWifi1", 0) / 1000;

        long totalDownMob = sp.getLong("totalDownMob1", 0) / 1000;
        long totalUpMob = sp.getLong("totalUpMob1", 0) / 1000;

        this.total.setText(getDataFormat(totalDownWifi));
        this.dUp.setText(getDataFormat(totalUpMob));
        this.dDown.setText(getDataFormat(totalDownMob));
        this.wUp.setText(getDataFormat(totalUpWifi));

        yData[0] = getPercentage(totalDownWifi, total);
        xData[0] = "WIFI download: " + String.format("%.2f", yData[0]) + "%";

        yData[1] = getPercentage(totalUpWifi, total);
        xData[1] = "WIFI upload: " + String.format("%.2f", yData[1]) + "%";

        yData[2] = getPercentage(totalDownMob, total);
        xData[2] = "Mobile data download: " + String.format("%.2f", yData[2]) + "%";

        yData[3] = getPercentage(totalUpMob, total);
        xData[3] = "Mobile data upload: " + String.format("%.2f", yData[3]) + "%";
    }

    private String getDataFormat(long total) {
        if(total > 999){
            return (String.format("%.2f", ((float) total / (float)1000))) + "GB. ";
        }else{
            return "" + total + "MB. ";
        }

    }

    public static float getPercentage(long n, long total) {
        float proportion = ((float) n) / ((float) total);
        return proportion * 100;
    }

    private void addData() {
        ArrayList<Entry> yVasl = new ArrayList<Entry>();
        for(int t = 0; t < yData.length; t++){
            yVasl.add(new Entry(yData[t], t));
        }

        ArrayList<String> xVasl = new ArrayList<String>();
        for(int t = 0; t < xData.length; t++){
            xVasl.add(xData[t]);
        }

        PieDataSet dataSet = new PieDataSet(yVasl, "Overview");
        dataSet.setSliceSpace(3);
        dataSet.setDrawValues(false);
        dataSet.setSelectionShift(5);
        dataSet.setValueTextColor(Color.WHITE);
        dataSet.setValueTextSize(20f);

        ArrayList<Integer> colors = new ArrayList<Integer>();

        colors.add(Color.parseColor("#FF0A89"));
        colors.add(Color.parseColor("#00A5EC"));
        colors.add(Color.parseColor("#198230"));
        colors.add(Color.parseColor("#773A80"));
        dataSet.setColors(colors);

        PieData data = new PieData(xVasl, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setDrawValues(false);
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        pChart.setData(data);
        pChart.highlightValues(null);
        pChart.invalidate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }

    public static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }


}
